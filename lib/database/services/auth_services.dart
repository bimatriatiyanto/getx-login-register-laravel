import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:products_youtube/database/models/user_model.dart';

class AuthServices {
  static String baseApi = "http://larave-auth.test/api";

  static var client = http.Client();

  static register({required name, required email, required password}) async {
    var response = await client.post(
      Uri.parse("$baseApi/register"),
      headers: {
        'Content-type': 'application/json',
        'Accept': 'application/json',
      },
      body: jsonEncode(
          <String, String>{"name": name, "email": email, "password": password}),
    );

    print(response.statusCode);
    print(jsonEncode(
        <String, String>{"name": name, "email": email, "password": password}));
    // print(name);
    // print(email);

    if (response.statusCode == 200 || response.statusCode == 201) {
      var stringObject = response.body;
      print(stringObject);
      var user = userModelFromJson(stringObject);
      return user;
    } else {
      return null;
    }
  }

  static login({required email, password}) async {
    var response = await client.post(
      Uri.parse("$baseApi/login"),
      headers: {'Content-Type': 'application/json'},
      body: jsonEncode(<String, String>{"email": email, "password": password}),
    );

    if (response.statusCode == 200 || response.statusCode == 201) {
      var stringObject = response.body;
      var user = userModelFromJson(stringObject);
      return user;
    } else {
      return null;
    }
  }
}
