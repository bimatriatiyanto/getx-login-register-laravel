import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
import 'package:products_youtube/routes/routes.dart';

class AuthController extends GetxController {
  var storage = const FlutterSecureStorage();
  var name = ''.obs;
  var token = '';

  @override
  void onInit() {
    authData();
    super.onInit();
  }

  Future<void> authData() async {
    if (token.isNotEmpty) {
      name.value = (await storage.read(key: "name"))!;
      token = (await storage.read(key: "token"))!;
    }
    name.value = (await storage.read(key: "name"))!;
    token = (await storage.read(key: "token"))!;
  }

  bool isAuth() {
    return token.isNotEmpty;
  }

  Future<void> doLogout() async {
    await storage.deleteAll();
    Get.toNamed(AppRoutes.login);
  }

  Future<void> readAll() async {
    name.value = (await storage.read(key: "name"))!;
    token = (await storage.read(key: "token"))!;
  }
}
